package com.cardiweb.eureka.client;

import java.util.List;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.cardiweb.eureka.client.external.model.User;

@FeignClient("application2")
public interface Client2API {

	  @RequestMapping(value = "/test-app2/{value}", method = RequestMethod.GET)
	  public String testApplication2(@PathVariable("value") String value);

	  @RequestMapping(value = "/users?role=USER", method = RequestMethod.GET)
	  public List<User> getUsersNotAdmin();

	  @RequestMapping(value = "/users/{userId}", method = RequestMethod.DELETE)
	  public void deleteUser(@PathVariable("userId") Long userId);
}
