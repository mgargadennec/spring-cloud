package com.cardiweb.eureka.client;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.FeignClientScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cardiweb.eureka.client.external.model.User;
import com.netflix.discovery.DiscoveryClient;


@Configuration
@EnableAutoConfiguration
@RestController
@EnableDiscoveryClient
@FeignClientScan
public class Client1 {

  public static void main(String[] args) {
    SpringApplication.run(Client1.class, args);
  }
  
  @Autowired
  DiscoveryClient discoveryClient;
  
  @Autowired
  Client2API client2Api;

  @RequestMapping(value = "/test-app1/{value}", method = RequestMethod.GET)
  public String testApplication1(@PathVariable("value") String value, HttpServletRequest req){
	  return "from app1 : "+value+" --> "+req.getRequestURL();
  }

  @RequestMapping(value = "/link/{testValue}", method = RequestMethod.GET)
  public String testApplication2(@PathVariable("testValue") String testValue){
	  return  client2Api.testApplication2(testValue);
  }

  @RequestMapping(value = "/users", method = RequestMethod.GET)
  public List<User> testGetUser(){
	  return client2Api.getUsersNotAdmin();
  }
  
  @RequestMapping(value = "/deleteUser/{id}", method = RequestMethod.GET)
  public void testGet(@PathVariable("id") Long id){
	  client2Api.deleteUser(id);
  }

}