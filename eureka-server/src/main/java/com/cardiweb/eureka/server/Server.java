package com.cardiweb.eureka.server;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Maps;
import com.netflix.discovery.DiscoveryClient;
import com.netflix.discovery.shared.Application;


@Configuration
@EnableAutoConfiguration
@RestController
@EnableEurekaServer
public class Server {

  public static void main(String[] args) {
    SpringApplication.run(Server.class, args);
  }
  
  @Autowired
  DiscoveryClient discoveryClient;
  
  @RequestMapping("/applications")
  public Map<String,String> getRandomApplications(){
	  Map<String,String> urlByApplication = Maps.newHashMap();
	  List<Application> applications = discoveryClient.getApplications().getRegisteredApplications();

	  for (Application application : applications) {
	    urlByApplication.put(application.getName(),discoveryClient.getNextServerFromEureka(application.getName(),false).getHomePageUrl());
	  }	  
	  return urlByApplication;
  }


}