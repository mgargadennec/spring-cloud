package com.cardiweb.eureka.client.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.cardiweb.eureka.client.dao.UserRepository;
import com.cardiweb.eureka.client.model.User;

@RestController
@RequestMapping("/users")
public class UserController {

	private UserRepository userRepository;

	@Autowired
	public UserController(UserRepository userRepository){
		this.userRepository = userRepository;
	};

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> createUser(@RequestBody User user){
		User savedUser = userRepository.save(user);

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setLocation(ServletUriComponentsBuilder
				.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedUser.getId()).toUri());

		return new ResponseEntity<>(null, httpHeaders, HttpStatus.CREATED);
	}	

	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody Iterable<User> getUsers(@RequestParam(value="role",required = false) final String role){
		if(role ==null){
			return userRepository.findAll();
		}else{
			return userRepository.findByRole(role);
		}
	}

	@RequestMapping(value="/{userId}",method = RequestMethod.GET)
	public @ResponseBody User getUser(@PathVariable("userId") Long userId, HttpServletResponse res){
		return getUser(userId);
	}

	@RequestMapping(value="/{userId}",method = RequestMethod.DELETE)
	public void deleteUser(@PathVariable("userId") Long userId){
		userRepository.delete(getUser(userId));
	}

	private User getUser(Long userId) {
		User user = userRepository.findOne(userId);
		if(user == null){
			throw new UserNotFoundException(userId);
		}
		return user;
	}
}



@ResponseStatus(HttpStatus.NOT_FOUND)
class UserNotFoundException extends RuntimeException {

	public UserNotFoundException(Long userId) {
		super("Could not find user '" + userId + "'.");
	}
}
