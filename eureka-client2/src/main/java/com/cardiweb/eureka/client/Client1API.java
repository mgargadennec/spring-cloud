package com.cardiweb.eureka.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.netflix.ribbon.eureka.EurekaRibbonClientConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("application1")
@RibbonClient(name = "application1", configuration = EurekaRibbonClientConfiguration.class)
public interface Client1API {
	
	  @RequestMapping(value = "/test-app1/{value}", method = RequestMethod.GET)
	  public String testApplication1(@PathVariable("value") String value);

}
