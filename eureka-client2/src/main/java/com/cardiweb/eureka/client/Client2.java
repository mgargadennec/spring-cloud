package com.cardiweb.eureka.client;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.FeignClientScan;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.netflix.ribbon.eureka.EurekaRibbonClientConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cardiweb.eureka.client.dao.UserRepository;
import com.cardiweb.eureka.client.model.User;
import com.google.common.collect.Lists;
import com.netflix.discovery.DiscoveryClient;


@Configuration
@EnableAutoConfiguration
@RestController
@EnableDiscoveryClient
@FeignClientScan
@ComponentScan("com.cardiweb.eureka.client")
public class Client2 {

	  @Autowired
	  DiscoveryClient discoveryClient;

	  @Autowired
	  Client1API client1Api;
	  
	  @Autowired
	  Client2API client2Api;
	  
  public static void main(String[] args) {
	  ApplicationContext applicationContext = SpringApplication.run(Client2.class, args);
    
	  List<User> users = Lists.newArrayList();
	  users.add(new User("admin", "ADMIN"));
	  users.add(new User("user1", "USER"));
	  users.add(new User("user2", "USER"));
	  users.add(new User("user3", "USER"));
	  users.add(new User("user4", "USER"));
	  users.add(new User("user5", "USER"));
	  users.add(new User("user6", "USER"));
	  
	  UserRepository userRepository = applicationContext.getBean(UserRepository.class);
	  userRepository.save(users);
  }
  
  @RequestMapping(value = "/test-app2/{value}", method = RequestMethod.GET)
  public String testApplication2(@PathVariable("value") String value, HttpServletRequest req){
	  return "from app2 : "+value+" --> "+req.getRequestURL();
  }
  
  @RequestMapping(value = "/link/{testValue}", method = RequestMethod.GET)
  public String testApplication1(@PathVariable("testValue") String testValue){
	  return  client1Api.testApplication1(testValue);
  }

  @RequestMapping(value = "/selflink/{testValue}", method = RequestMethod.GET)
  public String testDoubleCall(@PathVariable("testValue") String testValue){
	  return  client1Api.testApplication1(testValue)+" || "+client2Api.testApplication2(testValue);
  }
  

}