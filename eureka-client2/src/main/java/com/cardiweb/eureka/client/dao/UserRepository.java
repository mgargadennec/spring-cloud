package com.cardiweb.eureka.client.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.cardiweb.eureka.client.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
	
	public List<User> findByRole(String role);
}
