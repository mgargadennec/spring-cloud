package com.cardiweb.eureka.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.netflix.ribbon.eureka.EurekaRibbonClientConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("application2")
@RibbonClient(name = "application2", configuration = EurekaRibbonClientConfiguration.class)
public interface Client2API {

	  @RequestMapping(value = "/test-app2/{value}", method = RequestMethod.GET)
	  public String testApplication2(@PathVariable("value") String value);
}
